<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、MySQL、テーブル接頭辞、秘密鍵、言語、ABSPATH の設定を含みます。
 * より詳しい情報は {@link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 * wp-config.php の編集} を参照してください。MySQL の設定情報はホスティング先より入手できます。
 *
 * このファイルはインストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さず、このファイルを "wp-config.php" という名前でコピーして直接編集し値を
 * 入力してもかまいません。
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'zombieee_landdb');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'zombieee');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'azara4azara4');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql314.db.sakura.ne.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tiV1N%*N#?y/(ZLq42x.i6Wf!X3/qna7f(+nppq7~|8jK;7B?;[o@eyikCFY|tUz');
define('SECURE_AUTH_KEY',  '$c:eAgii2O{X6DBFujX+5!J8|2f+MJ9]/fiN:AVW,F1_Fa;.T&Rw!RKDq{(>/7we');
define('LOGGED_IN_KEY',    'oboK*nq41V7%;#[9Dmxcfv>B`-3Z-5)U-Ag=_Ra(uRGIO,=3@*gL-syv<Y5hB5H)');
define('NONCE_KEY',        'Uw%6oi[0Kkw)Ndnv39)1R-D{`]b-~hYO3u%~L<kInp[p|ooe6$]26}mI%2>zedp:');
define('AUTH_SALT',        'KVN#50Idz.0 :wLS:^8J,:J>27s_81e ;jS4%ESKW%h>U`@.QjdNz/>@lR|fAj/@');
define('SECURE_AUTH_SALT', ')VA-++j+pczHao|9P4$;1TihIe>?iP+6dd)[aUP&.jh+7U0.L(?O1FfOc _r}Dv5');
define('LOGGED_IN_SALT',   '&utb~wg_%SLf!BtS_oN-g|RqaglTJ*GXN^mX>+kpJd/6!0:gt{9B,/Hg;#dPeRjC');
define('NONCE_SALT',       '#Gt30Z/UDRQvSgd|!Zb+{mqs1;?yKY+cuf49Uw0W0X{v|V-nJ|=c(}#3*oof#=0 ');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp1_';

/**
 * ローカル言語 - このパッケージでは初期値として 'ja' (日本語 UTF-8) が設定されています。
 *
 * WordPress のローカル言語を設定します。設定した言語に対応する MO ファイルが
 * wp-content/languages にインストールされている必要があります。たとえば de_DE.mo を
 * wp-content/languages にインストールし WPLANG を 'de_DE' に設定すると、ドイツ語がサポートされます。
 */
define('WPLANG', 'ja');

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('CONCATENATE_SCRIPTS', false );

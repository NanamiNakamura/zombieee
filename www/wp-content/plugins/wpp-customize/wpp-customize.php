<?php
/*
Plugin Name: WPP Customize 
Plugin URI: http://takahashi-netbusiness.com/30/
Description: WPPランキングのカスタマイズ
Author: Takahashi
Version: 1.0
Author URI: http://takahashi-netbusiness.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

class WppCustomize {

	function __construct() {

		add_filter('wpp_post',array($this,'custom_html'),10,3);

	}


	private function get_customfield($p,$content){

		while( preg_match('/\{[^ \r\t\n\f\v^\}]*\}/', $content, $m) == 1 ){
			$field = str_replace('}','',str_replace('{','',$m[0]));
			$field = get_post_meta($p->id,$field,true);
			$content = str_replace($m[0],$field,$content);
		}

		return $content;

	}

	function custom_html( $content ,$p, $instance ){

		return $this->get_customfield($p,$content);

	}

}

$WppCustomize = new WppCustomize;

?>